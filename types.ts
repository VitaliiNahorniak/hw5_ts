import { Institute } from "./enums"
import { Person, Grades } from "./interfaces"

// Custom Types
export type Employee = Person & { institute: Institute; salary: number };

export type Student = Person & { institute: Institute; stipend: number };

// Utility Types
export type ReadonlyGrades = Readonly<Grades>;