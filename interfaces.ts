// Interfaces
export interface Person {
    name: string;
    age: number;
}

export interface interfaceUniversity {
    name: string;
    city?: string;
}

export interface Grades {
    subject?: string;
    value: number;
}

// Type Assertions
export interface SpecialPerson extends Person {
    illness: boolean;
}