export enum Profession {
    Director = 'DIRECTOR',
    Dean = 'DEAN',
    Teacher = 'TEACHER',
    Student = 'STUDENT',
    Cleaner = 'CLEANER',
    Security = 'SECURITY',
}

export enum Institute {
    Economy = 'ECONOMY',
    Tourism = 'TOURISM',
    Energetics = 'ENERGETICS',
}

export enum Rating {
    Three = 51,
    Four = 71,
    Five = 88,
}

export enum Stipend {
    Standart = 1000,
    High = 1500,
    Empty = 0,
}