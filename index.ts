import { Institute, Rating } from "./enums"
import { Person } from "./interfaces"
import * as classes from "./classes"

// Testing our implementation
const Politech = new classes.University('Politech','Lviv');

//Create Director and Institutes
const DirectorMain = new classes.Director('Roman Romanovych', 77);

const InstituteOfEconomy = new classes.instituteBase<Person>(Institute.Economy);
const InstituteOfTourism = new classes.instituteBase<Person>(Institute.Tourism);
const InstituteOfEnergetics = new classes.instituteBase<Person>(Institute.Energetics);

console.log('');

Politech.addDirector(DirectorMain);
Politech.addInstitute(InstituteOfEconomy);
Politech.addInstitute(InstituteOfTourism);
Politech.addInstitute(InstituteOfEnergetics);

//console.log(`We create a new institute ${institute.name}`)
//console.log(Politech.director);
//console.log(Politech.institutes);
console.log('');

//Create Employees
const Dean1 = new classes.DeanOfEconomy('Kateryna', 40);
const TeacherOfEconomy1 = new classes.TeacherOfEconomy('Oleg', 42);
const TeacherOfEconomy2 = new classes.TeacherOfEconomy('Inna', 44);
const CleanerOfEconomy1 = new classes.CleanerOfEconomy('Tamara', 55);
const SecurityOfEconomy1 = new classes.SecurityOfEconomy('Ivan', 60);

const Dean2 = new classes.DeanOfTourism('Vasyl', 30);
const TeacherOfTourism1 = new classes.TeacherOfTourism('Petro', 31);
const TeacherOfTourism2 = new classes.TeacherOfTourism('Lubov', 32);
const CleanerOfTourism1 = new classes.CleanerOfTourism('Nadia', 33);
const SecurityOfTourism1 = new classes.SecurityOfTourism('Denys', 34);

const Dean3 = new classes.DeanOfEnergetics('Nina', 50);
const TeacherOfEnergetics1 = new classes.TeacherOfEnergetics('Myron', 51);
const TeacherOfEnergetics2 = new classes.TeacherOfEnergetics('Mykola', 52);
const CleanerOfEnergetics1 = new classes.CleanerOfEnergetics('Oles', 53);
const SecurityOfEnergetics1 = new classes.SecurityOfEnergetics('Gena', 54);

InstituteOfEconomy.addDean(Dean1);
InstituteOfEconomy.addEmployee(TeacherOfEconomy1);
InstituteOfEconomy.addEmployee(TeacherOfEconomy2);
InstituteOfEconomy.addEmployee(CleanerOfEconomy1);
InstituteOfEconomy.addEmployee(SecurityOfEconomy1);

InstituteOfTourism.addDean(Dean2);
InstituteOfTourism.addEmployee(TeacherOfTourism1);
InstituteOfTourism.addEmployee(TeacherOfTourism2);
InstituteOfTourism.addEmployee(CleanerOfTourism1);
InstituteOfTourism.addEmployee(SecurityOfTourism1);

InstituteOfEnergetics.addDean(Dean3);
InstituteOfEnergetics.addEmployee(TeacherOfEnergetics1);
InstituteOfEnergetics.addEmployee(TeacherOfEnergetics2);
InstituteOfEnergetics.addEmployee(CleanerOfEnergetics1);
InstituteOfEnergetics.addEmployee(SecurityOfEnergetics1);
console.log('');

//1st September
console.log('1st September -> Start of study\r\n');

const StudentOfEconomy1 = new classes.StudentOfEconomy('Din1', 18);
const StudentOfEconomy2 = new classes.StudentOfEconomy('John1', 19);
const StudentOfEconomy3 = new classes.StudentOfEconomy('Jack1', 18);
const StudentOfEconomy4 = new classes.StudentOfEconomy('Lina1', 19);
const StudentOfEconomy5 = new classes.StudentOfEconomy('Bob1', 20);

const StudentOfTourism1 = new classes.StudentOfTourism('Din2', 18);
const StudentOfTourism2 = new classes.StudentOfTourism('John2', 19);
const StudentOfTourism3 = new classes.StudentOfTourism('Jack2', 18);
const StudentOfTourism4 = new classes.StudentOfTourism('Lina2', 19);
const StudentOfTourism5 = new classes.StudentOfTourism('Bob2', 20);

const StudentOfEnergetics1 = new classes.StudentOfEnergetics('Din3', 18);
const StudentOfEnergetics2 = new classes.StudentOfEnergetics('John3', 19);
const StudentOfEnergetics3 = new classes.StudentOfEnergetics('Jack3', 18);
const StudentOfEnergetics4 = new classes.StudentOfEnergetics('Lina3', 19);
const StudentOfEnergetics5 = new classes.StudentOfEnergetics('Bob3', 20);

InstituteOfEconomy.addStudent(StudentOfEconomy1);
InstituteOfEconomy.addStudent(StudentOfEconomy2);
InstituteOfEconomy.addStudent(StudentOfEconomy3);
InstituteOfEconomy.addStudent(StudentOfEconomy4);
InstituteOfEconomy.addStudent(StudentOfEconomy5);

InstituteOfTourism.addStudent(StudentOfTourism1);
InstituteOfTourism.addStudent(StudentOfTourism2);
InstituteOfTourism.addStudent(StudentOfTourism3);
InstituteOfTourism.addStudent(StudentOfTourism4);
InstituteOfTourism.addStudent(StudentOfTourism5);

InstituteOfEnergetics.addStudent(StudentOfEnergetics1);
InstituteOfEnergetics.addStudent(StudentOfEnergetics2);
InstituteOfEnergetics.addStudent(StudentOfEnergetics3);
InstituteOfEnergetics.addStudent(StudentOfEnergetics4);
InstituteOfEnergetics.addStudent(StudentOfEnergetics5);
console.log('');

//31 December (End of Session)

console.log('31 December -> End of Session\r\n');

//assessment
TeacherOfEconomy1.GradeForStudent(StudentOfEconomy1, 90);
TeacherOfEconomy1.GradeForStudent(StudentOfEconomy2, 80);
TeacherOfEconomy1.GradeForStudent(StudentOfEconomy3, 70);
TeacherOfEconomy1.GradeForStudent(StudentOfEconomy4, 60);
TeacherOfEconomy1.GradeForStudent(StudentOfEconomy5, 50);

TeacherOfEconomy2.GradeForStudent(StudentOfEconomy1, 90);
TeacherOfEconomy2.GradeForStudent(StudentOfEconomy2, 80);
TeacherOfEconomy2.GradeForStudent(StudentOfEconomy3, 70);
TeacherOfEconomy2.GradeForStudent(StudentOfEconomy4, 60);
TeacherOfEconomy2.GradeForStudent(StudentOfEconomy5, 50);


TeacherOfTourism1.GradeForStudent(StudentOfTourism1, 90);
TeacherOfTourism1.GradeForStudent(StudentOfTourism2, 80);
TeacherOfTourism1.GradeForStudent(StudentOfTourism3, 70);
TeacherOfTourism1.GradeForStudent(StudentOfTourism4, 60);
TeacherOfTourism1.GradeForStudent(StudentOfTourism5, 50);

TeacherOfTourism2.GradeForStudent(StudentOfTourism1, 90);
TeacherOfTourism2.GradeForStudent(StudentOfTourism2, 80);
TeacherOfTourism2.GradeForStudent(StudentOfTourism3, 70);
TeacherOfTourism2.GradeForStudent(StudentOfTourism4, 60);
TeacherOfTourism2.GradeForStudent(StudentOfTourism5, 50);


TeacherOfEnergetics1.GradeForStudent(StudentOfEnergetics1, 90);
TeacherOfEnergetics1.GradeForStudent(StudentOfEnergetics2, 80);
TeacherOfEnergetics1.GradeForStudent(StudentOfEnergetics3, 70);
TeacherOfEnergetics1.GradeForStudent(StudentOfEnergetics4, 60);
TeacherOfEnergetics1.GradeForStudent(StudentOfEnergetics5, 50);

TeacherOfEnergetics2.GradeForStudent(StudentOfEnergetics1, 90);
TeacherOfEnergetics2.GradeForStudent(StudentOfEnergetics2, 80);
TeacherOfEnergetics2.GradeForStudent(StudentOfEnergetics3, 70);
TeacherOfEnergetics2.GradeForStudent(StudentOfEnergetics4, 60);
TeacherOfEnergetics2.GradeForStudent(StudentOfEnergetics5, 50);
console.log('');

// Review stipends after session
StudentOfEconomy1.refreshStipend();
StudentOfEconomy2.refreshStipend();
StudentOfEconomy3.refreshStipend();
StudentOfEconomy4.refreshStipend();
StudentOfEconomy5.refreshStipend();

StudentOfTourism1.refreshStipend();
StudentOfTourism2.refreshStipend();
StudentOfTourism3.refreshStipend();
StudentOfTourism4.refreshStipend();
StudentOfTourism5.refreshStipend();

StudentOfEnergetics1.refreshStipend();
StudentOfEnergetics2.refreshStipend();
StudentOfEnergetics3.refreshStipend();
StudentOfEnergetics4.refreshStipend();
StudentOfEnergetics5.refreshStipend();
console.log('');

//Expel Students
console.log('Remove students\r\n');

if (StudentOfEconomy1.averageGrade < Rating.Three){
    InstituteOfEconomy.removeStudent('Din1');
}

if (StudentOfEconomy5.averageGrade < Rating.Three){
    InstituteOfEconomy.removeStudent('Bob1');
}

//Review salary 
console.log('Review salary \r\n');

DirectorMain.newSalaryForEmployee(TeacherOfTourism1, 6000);
DirectorMain.newSalaryForEmployee(TeacherOfTourism2, 7500);
DirectorMain.newSalaryForEmployee(CleanerOfEconomy1, 5500);
DirectorMain.newSalaryForEmployee(SecurityOfEnergetics1, 4500);
DirectorMain.newSalaryForEmployee(Dean3, 7559);
console.log('');

//Check
console.log('Check if person is student \r\n');
if (classes.isStudent(StudentOfEconomy1)) {
    console.log(`${StudentOfEconomy1.name} is a Student.`);
} else {
    console.log(`You are not a Student.`);
}

if (classes.isStudent(TeacherOfEnergetics1)) {
    console.log(`${TeacherOfEnergetics1.name} is a Student.`);
} else {
    console.log(`You are not a Student.`);
}
