import { Profession, Institute, Rating, Stipend } from "./enums"
import { Person, interfaceUniversity, Grades, SpecialPerson } from "./interfaces"
import { Employee, Student, ReadonlyGrades } from "./types"

// Abstract Class
abstract class EmployeeBase {
    private _salary: number;

    constructor(public name: string, public age: number, public profession: Profession, public institute: Institute) {
        this._salary = 0;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }

    abstract mySalary(): number;
}

abstract class StudentBase {
    private _stipend: number;
    public student: string;
    private _grades: Grades[] = [];
    public averageGrade: number = Rating.Four;

    constructor(public name: string, public age: number, public institute: Institute) {
        this._stipend = Stipend.Standart;
        this.student = Profession.Student;
    }

    get stipend(): number {
        return this._stipend;
    }

    refreshStipend(): void {
        const averageGrade = (this._grades.reduce((total, grade) => total + grade.value, 0)) / this._grades.length ;

        this.averageGrade = averageGrade;

        if(averageGrade < Rating.Four) {
            this._stipend = Stipend.Empty;
        }

        if(averageGrade >= Rating.Four && averageGrade < Rating.Five) {
            this._stipend = Stipend.Standart;
        }

        if(averageGrade >= Rating.Five) {
            this._stipend = Stipend.High;
        }
        
        console.log(`My avarage grades is ${averageGrade} and I will have stipend ${this._stipend}grn`)

    }

    get grades(): ReadonlyGrades[] {
        return this._grades;
    }

    addGrades(grades: Grades): void {
        this._grades.push(grades);
    }

    clearGrades(): void {
        this._grades = [];
    }
    
    abstract myStipend(): number;
}

//Class
export class Director {
    private _salary: number = 10000;
    public profession: string;

    constructor(public name: string, public age: number) {
        this._salary = 0;
        this.profession = Profession.Director;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }

    newSalaryForEmployee(employee: EmployeeBase, newValue: number) {
        employee.salary = newValue;
        console.log(`I give you a new salary ${newValue}`)
    }

}

// Class Inheritance
//Deans
export class DeanOfEconomy extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Dean, Institute.Economy);
    }

    mySalary(): number {
        return 5000;
    }
}

export class DeanOfTourism extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Dean, Institute.Tourism);
    }

    mySalary(): number {
        return 5000;
    }
}

export class DeanOfEnergetics extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Dean, Institute.Energetics);
    }

    mySalary(): number {
        return 5000;
    }
}

//Teachers
export class TeacherOfEconomy extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Teacher, Institute.Economy);
    }

    GradeForStudent(student: StudentOfEconomy, newValue: number) {
        const grade: Grades = { subject: Institute.Economy , value: newValue };
        student.addGrades(grade);
        console.log(`I give you a new grade ${newValue}`)
    }

    mySalary(): number {
        return 4000;
    }
}

export class TeacherOfTourism extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Teacher, Institute.Tourism);
    }

    GradeForStudent(student: StudentOfTourism, newValue: number) {
        const grade: Grades = { subject: Institute.Tourism , value: newValue };
        student.addGrades(grade);
        console.log(`I give you a new grade ${newValue}`)
    }

    mySalary(): number {
        return 4000;
    }
}

export class TeacherOfEnergetics extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Teacher, Institute.Energetics);
    }

    GradeForStudent(student: StudentOfEnergetics, newValue: number) {
        const grade: Grades = { subject: Institute.Energetics , value: newValue };
        student.addGrades(grade);
        console.log(`I give you a new grade ${newValue}`)
    }

    mySalary(): number {
        return 4000;
    }
}

//Cleaners
export class CleanerOfEconomy extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Cleaner, Institute.Economy);
    }

    mySalary(): number {
        return 3000;
    }
}

export class CleanerOfTourism extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Cleaner, Institute.Tourism);
    }

    mySalary(): number {
        return 3000;
    }
}

export class CleanerOfEnergetics extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Cleaner, Institute.Energetics);
    }

    mySalary(): number {
        return 3000;
    }
}

//Security
export class SecurityOfEconomy extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Security, Institute.Economy);
    }

    mySalary(): number {
        return 2000;
    }
}

export class SecurityOfTourism extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Security, Institute.Tourism);
    }

    mySalary(): number {
        return 2000;
    }
}

export class SecurityOfEnergetics extends EmployeeBase {
    constructor(name: string, age: number) {
        super(name, age, Profession.Security, Institute.Energetics);
    }

    mySalary(): number {
        return 2000;
    }
}

//Students
export class StudentOfEconomy extends StudentBase {
    constructor(name: string, age: number) {
        super(name, age, Institute.Economy);
    }

    myStipend(): number {
        return 0;
    }
}

export class StudentOfTourism extends StudentBase {
    constructor(name: string, age: number) {
        super(name, age, Institute.Tourism);
    }

    myStipend(): number {
        return 0;
    }
}

export class StudentOfEnergetics extends StudentBase {
    constructor(name: string, age: number) {
        super(name, age, Institute.Energetics);
    }

    myStipend(): number {
        return 0;
    }
}

// Generics
export class instituteBase<T extends Person> {
    private _dean: EmployeeBase[] = [];
    private _employees: Employee[] = [];
    private _students: T[] = [];


    constructor(public name: string) {
        this.name = name;
    }

    get dean(): EmployeeBase[] {
        return this._dean;
    }

    get employees(): Employee[] {
        return this._employees;
    }

    get students(): T[] {
        return this._students;
    }

    addDean(dean: EmployeeBase): void {
        this._dean.push(dean);
        console.log(`We have a new dean ${dean.name} in institute of ${dean.institute}`);
    }

    addEmployee(employee: EmployeeBase): void {
        this._employees.push(employee);
        console.log(`We have a new employee ${employee.name}! He/She is ${employee.profession} in institute of ${employee.institute}`);
    }

    addStudent(student: T): void {
        this._students.push(student);
        console.log(`We have a new student ${student.name}!`);
    }

    removeDean(): void {
        this._dean = [];
    }

    removeEmployee(name: string): void {
        this._employees = this._employees.filter((employee) => employee.name !== name);
    }

    removeStudent(name: string): void {
        this._students = this._students.filter((student) => student.name !== name);
    }

}

export class University{
    private _director: Director[] = [];
    private _institutes: interfaceUniversity[] = [];

    constructor(public name: string, public city: string) {
        this.name = name;
        this.city = city;
    }

    @LogGet
    get director(): Director[] {
        return this._director;
    }

    
    public addDirector(director: Director): void {
        this._director.push(director);
        console.log(`We have a new director ${director.name}`)
    }

    removeDirector(): void {
        this._director = [];
    }

    @LogGet
    get institutes(): interfaceUniversity[] {
        return this._institutes;
    }

    addInstitute(institute: interfaceUniversity): void {
        this._institutes.push(institute);
        console.log(`We create a new institute ${institute.name}`)
    }

}

//Decorators
function LogGet(target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor): void {
    console.log(`You can use getter - ${String(propertyKey)} of University`);
}



// Type Guards
export function isStudent(person: Person | StudentBase): person is StudentBase {
    return (person as StudentBase).averageGrade !== undefined;
}